from collections import defaultdict

def parse_list_as_dg(input_list):
    """
    Converts a list into a Directed Graph in the form of a dictionary.

    Parameters:
        input_list (list): Ordered list of nodes.

    Returns:
        dir_graph (dict): Dictionary of lists, where each key is a node in the graph.
                    Each key's value is a list of nodes that it points to.
    """
    dir_graph = defaultdict(list)
    for i in range(len(input_list)-1):
        dir_graph[input_list[i]].append(input_list[i+1])

    return dir_graph

def identify_router(dir_graph):
    """
    Finds the node(s) with maximum degree(total inbound and outbound connections).

    Parameters:
        dir_graph (dict): Directed Graph represented as a dictionary.

    Returns:
        max_nodes (list): List of node(s) with maximum degree.
    """
    degrees = defaultdict(int)
    for node, neighbors in dir_graph.items():
        degrees[node] += len(neighbors)
        for n in neighbors:
            degrees[n] += 1
    
    max_nodes = [keys for keys,values in degrees.items() if values == max(degrees.values())]
    return max_nodes

dir_graph1 = parse_list_as_dg([1,2,3,5,2,1])
dir_graph2 = parse_list_as_dg([1,3,5,6,4,5,2,6])
dir_graph3 = parse_list_as_dg([2,4,6,2,5,6])

print(identify_router(dir_graph1))
print(identify_router(dir_graph2))
print(identify_router(dir_graph3))

# Time Complexity:
# parse_list_as_dg() has a time complexity of O(N-1), where N is length of input list.
# identify_router() has a time complexity of O(N*M), where N is number of nodes and M is 
# number of neighbors of the node.