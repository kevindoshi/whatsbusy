def compress(s):
    """
    Returns a compressed version of a string, if possible.

    Parameters:
        s (str): String to be compressed.

    Returns:
        new_str (s): Compressed version of the string, if possible. 
                    Otherwise returns original string.
    """


    new_str = ""
    stack = []
    for i in range(len(s)):

        if not stack:   # If stack is empty, push current char to stack
            stack.append(s[i])
            continue

        else:   # If stack is not empty, check top of stack
            top = stack[-1]
            if top == s[i]: # If current char == previous char, push char to stack
                stack.append(s[i])

            elif top != s[i]:   # If current char != previous char, append char to new string
                new_str += top
                top = ""

                if len(stack) > 1: # Only append number if char has appeared more than once
                    new_str += str(len(stack))

                stack = []
                stack.append(s[i])
    
    if len(stack) > 0: # Check for chars at the end
        new_str += stack[-1]
        if len(stack) > 1:
            new_str += str(len(stack))
        
    if len(new_str) < len(s): # Only return new string if shorter than original string
        return new_str
    else:
        return s


assert compress('aaaabbbccddddddee') == 'a4b3c2d6e2'
assert compress('bbcceeee') == 'b2c2e4'
assert compress('aaabbbcccaaa') == 'a3b3c3a3'
assert compress('a') == 'a'

# Time Complexity for this code would be O(N + M*P)
# Here N is length of input string and M is number of times the concatenate operation is called,
# and P is length of string after concatenation. 